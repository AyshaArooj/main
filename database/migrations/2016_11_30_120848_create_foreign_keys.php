<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('jobs', function(Blueprint $table){

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });


            Schema::table('resumes',function (blueprint $table) {

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            });


            Schema::table('experiences', function (blueprint $table) {

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            });

            Schema::table('educations',function (blueprint $table){

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            });

            Schema::table('job_user_quote', function (Blueprint $table) {

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
