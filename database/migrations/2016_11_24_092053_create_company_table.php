<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('secp_id');
            $table->string('name');
            $table->string('industry');
            $table->string('ceo_name');
            $table->string('hr_name');
            $table->integer('cnic');
            $table->string('contact_person');
            $table->string('job_designation');
            $table->string('ownership');
            $table->string('product');
            $table->string('description');
            $table->string('address');
            $table->string('location');
            $table->string('origin_country');
            $table->string('email');
            $table->integer('phone_no');
            $table->string('company_url');
            $table->integer('no_of_offices');
            $table->integer('no_of_employees');
            $table->integer('year_of_operating');
            $table->string('fax');
            $table->string('logo');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
