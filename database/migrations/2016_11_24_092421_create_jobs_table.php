<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('job_title');
            $table->string('description');
            $table->string('skill');
            $table->string('city');
            $table->string('address');
            $table->string('job_type');
            $table->integer('job_rate');
            $table->string('gender');
            $table->string('experience');
            $table->dateTime('apply_before');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
