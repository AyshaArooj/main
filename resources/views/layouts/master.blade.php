
<!DOCTYPE html>
<html>
<head>
    <title>EasyJobs</title>
</head>
<body>

@include('partial.topIncludes')

@if(!Auth::check())
    <!--- Topbar Start for guest user --->
    <header id="topbar">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <a href="/"><img src="{{asset('images/logo.png')}}" id="logo"></a>

                </div>

                <div class="col-md-8">

                    <div class="left-menu">

                        `<div class="col-md-6 text-right">
                            <a href="#" class="link-item">How it works</a>
                            <a href="#" class="link-item">Contact us</a>
                        </div>

                        <div class="col-md-6">
                            <a href="sign-up"><button id="signup-btn" class="btn">Sign Up</button></a>
                            <a href="login"><button id="login-btn" class="btn">Login</button></a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </header>
    <!--- Topbar Start for logged in user --->
    @else

    @include('layouts.nav')

    <!--- Topbar End --->
@endif

@yield('content')

<!--- Footer start --->

<!--- Topbar End --->

<footer id="footer-section">

    <div class="container">

        <div class="row ">

            <div class="col-md-4">

                <p><strong>For Employers</strong></p>

                <p>
                    <a href="#" class="link-item">Try it Free</a><br>

                    <a href="#" class="link-item">Plans & Pricing</a><br>

                    <a href="#" class="link-item">Plans & Pricing</a><br>

                    <a href="#" class="link-item">Blog</a><br>

                    <a href="sign-up" class="link-item">Affliliate Sign Up</a><br>

                    <a href="login" class="link-item">Affliliate Login</a>

                </p>

                <p style="margin: 40px 0;">

                    <strong>For Remote Workers</strong><br>

                    <small><a href="#" class="link-item">Create Your Profile</a></small>

                </p>

            </div>

            <div class="col-md-4">

                <p class="text-left link-item" style=""><strong>Help</strong></p><br>

                <a href="#" class="link-item">Contact</a><br>

                <a href="#" class="link-item">Support</a><br>

                <a href="#" class="link-item">FAQ</a><br>

                <a href="#" class="link-item">Privacy Policy</a><br>

                <img  style="margin: 40px 0;" src="{{asset('images/footer-logo.png')}}">

            </div>

            <div class="col-md-4">

                <strong>Contact with Us</strong>

                <p>Contact Us: +012345678</p>

                <p>

                    <a href="#"><i class="fa fa-facebook" style="color: royalblue; font-size: 25px; margin: 0 10px;"></i></a>
                    <a href="#"><i class="fa fa-twitter" style="color: #28abe3; font-size: 25px; margin: 0 10px;"></i></a>
                    <a href="#"><i class="fa fa-pinterest-p" style="color: #c0392b; font-size: 25px; margin: 0 10px;"></i></a>
                    <a href="#"><i class="fa fa-google-plus" style="color: #e74c3c; font-size: 25px; margin: 0 10px;"></i></a>

                </p>

            </div>

        </div>


        <div class="row">

            <div class="col-md-12 text-center">

                <p>&copy; Easy Job Pvt Ltd 2007-2016. All Right Reseverd. EASYJOB &right; trademark of Easy job pvt Ltd.</p>

            </div>

        </div>

    </div>

</footer>
@include('partial.bottomIncludes')
</body>
<!--- Footer end --->
</html>

