<header id="topbar">

    <div class="container">

        <div class="row">

            <div class="col-md-3 col-sm-12">

                <a href="{{ route('dashboard')  }}"><img src="{{asset('images/logo.png')}}" id="logo"></a>

            </div>

            <div class="col-md-9 col-sm-12 text-right">

                <div class="left-menu">

                    <form class="form-inline">
                        <div class="col-md-10">

                            <input type="text" class="form-control top-search-bar" placeholder="I'm looking for a e.g (graphic desginer)">
                        </div>

                        <div class="col-sm-2">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {{ $user->email }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="{{ url('profile') }}">Profile</a></li>
                                    <li><a href="#">Setting</a></li>
                                    {{--<li role="separator" class="divider"></li>--}}
                                    <li><a href="{{ action('userController@logout') }}">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

    <section id="joblisting-topbar">

        <div class="container" style=" padding:0px;">

            <div class="row">

                <div class="col-sm-10 text-left">

                    <a href="{{url('create-job')}}" class="topbar-link-n">Post A Job</a>&nbsp; &nbsp; &nbsp;
                    <a href="{{url('add-company')}}" class="topbar-link-n">Add Company</a>&nbsp; &nbsp; &nbsp;
                    <a href="{{url('job-list')}}" class="topbar-link-n">Posted Job</a>&nbsp; &nbsp; &nbsp;
                    {{--<a href="job-detail" class="topbar-link-n">Job Detail</a>&nbsp; &nbsp; &nbsp;--}}
                    {{--<a href="find-job" class="topbar-link-n">Find Job</a>&nbsp; &nbsp; &nbsp;--}}

                </div>
            </div>
        </div>
    </section>

</header>