@extends('layouts.master')
@section('content')


    <div style="margin: 30px 0px; "></div>

    <section id="jobdetail">

        <div class="container">

            <div class="row">

                <div class="col-md-4 ">
                    <img src="{{asset('images/jobimg.jpg')}}" height="300px" width="360px">
                    <hr>
                    <img src="{{asset('images/jobimg2.jpg')}}" height="300px" width="360px">
                </div>

                <div class="col-md-8">
                    <div class="col-sm-8">
                        <h3>Job Details</h3>
                    </div>

                    <div class="col-sm-4">
                        <h4>Applications Received 0</h4>
                    </div>

                    <div class="jblk">

                        <div class="job-detail">

                            <table class="table table-striped table-responsive">

                                <tr>
                                    <th>Job Title:</th>
                                    <td>{{$jobDetail->job_title}}</td>
                                </tr>

                                <tr>
                                    <th>Skill:</th>
                                    <td>{{$jobDetail->skill}}</td>
                                </tr>

                                <tr>
                                    <th>City:</th>
                                    <td>{{$jobDetail->city}}</td>
                                </tr>

                                <tr>
                                    <th>Address</th>
                                    <td>{{$jobDetail->address}}</td>
                                </tr>

                                <tr>
                                    <th>Job Type:</th>
                                    <td>{{$jobDetail->job_type}}</td>
                                </tr>

                                <tr>
                                    <th>Job Rate</th>
                                    <td>{{$jobDetail->job_rate}}</td>
                                </tr>

                                <tr>
                                    <th>Gender</th>
                                    <td>{{$jobDetail->gender}}</td>
                                </tr>

                                <tr>
                                    <th>Experience</th>
                                    <td>{{$jobDetail->experience}}</td>
                                </tr>

                                <tr>
                                    <th>Apply Before</th>
                                    <td>{{$jobDetail->apply_before}}</td>
                                </tr>

                                <tr>
                                    <th>Posting Date</th>
                                    <td>{{$jobDetail->created_at}}</td>
                                </tr>

                                <tr>
                                    <th>Job Description:</th>
                                    <td style="word-break: break-word;">{{$jobDetail->description}}</td>
                                </tr>

                            </table>

                           {{--<p>&nbsp</p>--}}
                           {{--<p>&nbsp</p>--}}

                            {{--<a href="job-detail">--}}
                                {{--<button class="btn btn-info btn-md" style="width: 250px;" onclick="return jobDetail();">Apply</button>--}}
                            {{--</a>--}}

                     </div>
                </div>
            </div>
        </div>
      </div>
    </section>

    <div style="margin: 30px 0px;"></div>

@endsection
