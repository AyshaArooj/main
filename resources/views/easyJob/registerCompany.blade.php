{{--@extends('layouts.master')--}}
@section('content')

    <section id="login-section">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="form-box-company">
                        @if(session('success'))
                        <div class="alert alert-success">
                        {{ session('success') }}
                        </div>
                        @endif
                        <form action="{{action('easyJobController@postAddCompany')}}" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h4>Add Company:</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Name: </label>
                                        <input class="form-control" type="text" name="c-name" placeholder="" id="">
                                        @if ($errors->has('c-name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Industry: </label>
                                        <input class="form-control" type="text" name="industry"
                                               placeholder="" id="">
                                        @if ($errors->has('industry'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('industry') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> CEO of Company: </label>
                                            <input class="form-control" type="text" name="ceo" placeholder="" id="">
                                            @if ($errors->has('ceo'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('ceo') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Head HR Department: </label>
                                        <input class="form-control" type="text" name="head-hrd"
                                               placeholder="" id="">
                                        @if ($errors->has('head-hrd'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('head-hrd') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                             </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Job Designation: </label>
                                            <input class="form-control" type="text" name="designation"
                                                   placeholder="" id="">
                                            @if ($errors->has('designation'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('designation') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact Email: </label>
                                            <input class="form-control" type="text" name="contact-email"
                                                   placeholder="" id="">
                                            @if ($errors->has('contact-email'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('contact-email') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact Person Name: </label>
                                            <input class="form-control" type="text" name="contact-person"
                                                   placeholder="" id="">
                                            @if ($errors->has('contact-person'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('contact-person') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Url: </label>
                                            <input class="form-control" type="text" name="company-url"
                                                   placeholder="" id="">
                                            @if ($errors->has('company-url'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('company-url') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Product / Service: </label>
                                        <input class="form-control" type="text" name="c-product"
                                               placeholder="" id="">
                                        @if ($errors->has('c-product'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-product') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Ownership Type:</label>
                                        <select class="form-control" type="text" name="ownership-type" id="">
                                            <option value="sole">Sole</option>
                                            <option value="private">Private</option>
                                            <option value="public">Public</option>
                                            <option value="ngo">NGO</option>
                                        @if ($errors->has('ownership-type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ownership-type') }}</strong>
                                            </span>
                                        @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Address:</label>
                                        <input class="form-control" type="text" name="c-address"
                                               placeholder="" id="">
                                        @if ($errors->has('c-address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Location:</label>
                                        <input class="form-control" type="text" name="c-location" id="">
                                    @if ($errors->has('c-location'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-location') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Origin Of Company: </label>
                                        <input class="form-control" type="text" name="c-origin"
                                               placeholder="" id="">
                                        @if ($errors->has('c-origin'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-origin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Of Offices:</label>
                                        <input class="form-control" type="text" name="office-count" id="">
                                            @if ($errors->has('office-count'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('office-count') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Ph #: </label>
                                        <input class="form-control" type="text" name="c-phone"
                                               placeholder="" id="">
                                        @if ($errors->has('c-phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Fax:</label>
                                        <input class="form-control" type="text" name="c-fax" id="">
                                            @if ($errors->has('c-fax'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('c-fax') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No of Employees: </label>
                                        <input class="form-control" type="text" name="workers-count"
                                               placeholder="" id="">
                                        @if ($errors->has('workers-count'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('workers-count') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Year of Operating:</label>
                                        <input class="form-control" type="text" name="operating-years" id="">
                                            @if ($errors->has('operating-years'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('operating-years') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Logo: </label>
                                        <input class="form-control" type="text" name="logo"
                                               placeholder="" id="">
                                        @if ($errors->has('logo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('logo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Secp Company ID:</label>
                                        <input class="form-control" type="text" name="secp-id" id="">
                                            @if ($errors->has('secp-id'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('secp-id') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                        <label>Company Description: </label>
                                        <textarea class="form-group ajax-content" style="width: 760px;" rows="3"  name="c-description" placeholder="" id="">
                                        @if ($errors->has('c-description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('c-description') }}</strong>
                                            </span>
                                        @endif
                                        </textarea>
                                </div>
                            </div>

                            <button class="btn btn-info btn-md" style="width: 250px">Save New Company</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

