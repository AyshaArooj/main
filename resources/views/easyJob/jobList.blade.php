@extends('layouts.master')
@section('content')

    <!--- Joblist section Start --->

    <section id="joblist-section">

        <div class="container">

            <h3>Active Jobs</h3>

            @if(count($jobLists) > 0)
                @foreach($jobLists as $jobList)
                    <div class="job-list-bar">
                        <div class="row">

                            <a href="{{ url('job-detail') }}">

                                <div class="col-md-6">

                                    <div id="jobname">
                                        <h3>{{$jobList->job_title}}</h3>
                                        <small id="text-muted">ALNAFAYTECH</small>
                                    </div>

                                </div>

                                <div class="col-md-6 text-right">

                                    <div id="jobinfo">

                                        <i class="fa fa-globe"></i> &nbsp;
                                        <i class="fa fa-calendar"></i> {{$jobList->created_at}}
                                        <i class="fa fa-file-text-o"></i> &nbsp;Received

                                    </div>

                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            @else
                <h3><strong>No Active Jobs Found!</strong></h3>

            @endif

        </div><!-- Container End -->

    </section>


    <section id="featured-jobs">

        <div class="container">

            <h3>Pending Approval</h3>

            <div class="row">

                <div class="col-md-2 col-sm-12">

                    <button class="btn btn-info">Freelancer</button>

                </div>

                <div class="col-md-4 col-sm-12">

                    <strong>Web Designers</strong><br>
                    <small>AlNafayTech desinger</small>

                </div>

                <div class="col-md-4 col-sm-12s">

                    <strong>Gulberg Lahore</strong><br>
                    <small>Punjab, Pakistan</small>

                </div>

                <div class="col-md-2 col-sm-12 text-right">

                    <strong>23 Nov</strong><br>
                    <small>2016</small>

                </div>

            </div>

            <hr>

            <div class="row">

                <div class="col-md-2 col-sm-12">

                    <button class="btn btn-info">Freelancer</button>

                </div>

                <div class="col-md-4 col-sm-12">

                    <strong>Graphics Designer</strong><br>
                    <small>New media desinger</small>

                </div>

                <div class="col-md-4 col-sm-12">

                    <strong>Canada</strong><br>
                    <small>California United States</small>

                </div>

                <div class="col-md-2 col-sm-12 text-right">

                    <strong>12 Nov</strong><br>
                    <small>2016</small>

                </div>
            </div>
        </div>
    </section>

    <!--- Joblist Section End --->

    <!--- Pagination Start --->

    <section id="pagination-section">

        <div class="container text-right">

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>

    </section>
@endsection
<!--- Pagination End --->
