@extends('layouts.master')
@section('content')


    <div style="margin: 30px 0px; "></div>

    <section id="jobdetail">

        <div class="container">

            <div class="row">

                <div class="col-md-2 ">

                </div>

                <div class="col-md-8">

                    <div class="alert alert-info">
                    <strong>Your Job is in Admin Approval list</strong>
                    </div>

                    <div class="jblk"><h3>Job Preview</h3>

                        <div class="job-preview">

                            <table class="table table-striped table-responsive">


                                <tr>
                                    <th>Job Ttile:</th>
                                    <td>{{$job->job_title}}</td>
                                </tr>

                                <tr>
                                    <th>Skill:</th>
                                    <td>{{$job->skill}}</td>
                                </tr>

                                <tr>
                                    <th>City:</th>
                                    <td>{{$job->city}}</td>
                                </tr>

                                <tr>
                                    <th>Address</th>
                                    <td>{{$job->address}}</td>
                                </tr>

                                <tr>
                                    <th>Job Type:</th>
                                    <td>{{$job->job_type}}</td>
                                </tr>

                                <tr>
                                    <th>Job Rate</th>
                                    <td>{{$job->job_rate}}</td>
                                </tr>

                                <tr>
                                    <th>Gender</th>
                                    <td>{{$job->gender}}</td>
                                </tr>

                                <tr>
                                    <th>Experience</th>
                                    <td>{{$job->experience}}</td>
                                </tr>

                                <tr>
                                    <th>Apply Before</th>
                                    <td>{{$job->apply_before}}</td>
                                </tr>

                                <tr>
                                    <th>Posting Date</th>
                                    <td>{{$job->created_at}}</td>
                                </tr>

                                <tr>
                                    <th>Job Description:</th>
                                    <td style="word-break: break-word;">{{$job->description}}</td>
                                </tr>

                            </table>
                            {{--<a href="job-list"><button class="btn btn-info btn-md" style="width: 250px;">Reserve</button></a>--}}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div style="margin: 30px 0px;"></div>

@endsection
