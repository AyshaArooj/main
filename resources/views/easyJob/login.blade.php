@extends('layouts.master')
@section('content')

<!--- Login Section Start --->

<section id="login-section">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="form-box">
                    @if(session('fail'))
                        <div class="alert alert-danger">
                            {{ session('fail') }}
                        </div>
                    @endif
                    <h4>Remote Worker Login</h4>

                    <p><small>Get a full time or part time job today!</small></p>

                    <p><i class="fa fa-user" aria-hidden="true"></i></p>

                    <form action="{{action('userController@postLogin')}}" method="post">
                        {{csrf_field()}}
                        <div class="inner-addon left-addon{{ $errors->has('email') ? ' has-error' : '' }}">
                            <i class="glyphicon glyphicon glyphicon-envelope"></i>
                            <input type="email" name="email" class="form-control" placeholder="Email:"required />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                            @endif
                        </div>
                        <br>

                        <div class="inner-addon left-addon{{ $errors->has('password') ? ' has-error' : '' }}">
                            <i class="glyphicon glyphicon glyphicon glyphicon-lock"></i>
                            <input type="password" name="password" class="form-control" placeholder="Password:" required/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                            @endif
                        </div>

                        <div class="row">

                            <div class="col-md-12 col-sm-12">

                                <input type="submit" name="login" value="Login" class="form-control button">

                            </div>

                        </div>

                    </form>

                    <p>Don't have an account?</p>
                    <a href="{{ url('sign-up') }}" class="pull-left a">Sign Up</a>

                    <a href="#" class="pull-right a">Forget your password?</a>

                </div>

            </div>

        </div>

    </div>

</section>
<!--- Login Section End --->

@endsection