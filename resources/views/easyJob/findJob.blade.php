@extends('layouts.master')
@section('content')

{{--<!--- Topbar Start --->--}}

<!--- Topbar End --->

<section id="joblisting-topbar">

    <div class="container">

        <div class="row">

            <div class="col-md-8 ">

                <a href="#" class="topbar-link-n">How its works</a>

            </div>

            <div class="col-md-4 text-right">

                <button class="btn btn-bl">Find Work</button> &nbsp; &nbsp;

            </div>

        </div>

    </div>

</section>

<!--- Banner image --->

<section id="find-job-banner">


    <div class="find-job-note">

        <h2 style="font-weight: 100;">Find Your <strong>Dream Job</strong> </h2>

        <h2 style="letter-spacing: 65px;">ONLINE</h2>

    </div>

</section>

<!--- User Func ---->

<section id="user-func">

    <div class="container">

        <div class="row text-center user-func-banner">

            <div class="col-md-3 col-sm-12 col col-s">

                <i class="fa fa-pencil-square"></i>

                <p>Create your account</p>

            </div>

            <div class="col-md-3 col-sm-12 col">

                <i class="fa fa-industry"></i>

                <p>Select Your Industry</p>

            </div>

            <div class="col-md-3 col-sm-12 col">

                <i class="fa fa-user"></i>

                <p>Become a Member</p>

            </div>

            <div class="col-md-3 col-sm-12 col">

                <i class="fa fa-eye"></i>

                <p>Identity Check</p>

            </div>

        </div>

    </div>

</section>

<!--- All Jobs Text Area --->

<section id="find-job-form">

    <div class="container">

        <form class="">

            <div class="row text-center">

                <div class="col-md-5 col-sm-12">

                    <div class="form-group">

                        <input type="text" placeholder="All Jobs" class="form-control fj-f">

                    </div>

                </div>

                <div class="col-md-5 col-sm-12">

                    <div class="form-group">

                        <input type="text" placeholder="Location" class="form-control fj-s">

                    </div>

                </div>

                <div class="col-md-2">

                    <div class="form-group">

                        <button class="btn btn-info form-control fj-btn"><i class="fa fa-search"></i></button>

                    </div>

                </div>

            </div>

        </form>
    </div>

</section>

<section id="featured-jobs">

    <div class="container">

        <h3>Featured Jobs</h3>


        <div class="row">

            <div class="col-md-2 col-sm-12">

                <button class="btn btn-info">Freelancer</button>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Web Designers</strong><br>
                <small>AlNafayTech desinger</small>

            </div>

            <div class="col-md-4 col-sm-12s">

                <strong>Gulberg Lahore</strong><br>
                <small>Punjab, Pakistan</small>

            </div>

            <div class="col-md-2 col-sm-12 text-right">

                <strong>23 Nov</strong><br>
                <small>2016</small>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-2 col-sm-12">

                <button class="btn btn-info">Freelancer</button>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Graphics Designer</strong><br>
                <small>New media desinger</small>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Canada</strong><br>
                <small>California United States</small>

            </div>

            <div class="col-md-2 col-sm-12 text-right">

                <strong>12 Nov</strong><br>
                <small>2016</small>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-2 col-sm-12">

                <button class="btn btn-info">Freelancer</button>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Creating Designers</strong><br>
                <small>New media desinger</small>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Los Angeles</strong><br>
                <small>California United States</small>

            </div>

            <div class="col-md-2 col-sm-12 text-right">

                <strong>30 Oct</strong><br>
                <small>2016</small>

            </div>

        </div>

    </div><!--- Container end -->

</section>


<section id="latest-jobs">

    <div class="container">

        <h3>Latest Jobs</h3>


        <div class="row">

            <div class="col-md-2 col-sm-12">

                <button class="btn btn-warning">Full Time</button>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Refrigeration Repair Technician</strong><br>
                <small><u>Sears Crop - Posted by appthemedemo</u></small>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Austing</strong><br>
                <small>Texas USA</small>

            </div>

            <div class="col-md-2 col-sm-12 text-right">

                <strong>23 Nov</strong><br>
                <small>2016</small>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-2 col-sm-12">

                <button class="btn btn-success">Freelancer</button>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Sr. Finance Manager</strong><br>
                <small><u>Charls Schwab</u> - Posted </small>

            </div>

            <div class="col-md-4 col-sm-12">

                <strong>Canada</strong><br>
                <small>California United States</small>

            </div>

            <div class="col-md-2  col-sm-12 text-right">

                <strong>12 Nov</strong><br>
                <small>2016</small>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-2">

                <button class="btn btn-danger">Temporory</button>

            </div>

            <div class="col-md-4">

                <strong>Creating Designers</strong><br>
                <small>New media desinger</small>

            </div>

            <div class="col-md-4">

                <strong>Los Angeles</strong><br>
                <small>California United States</small>

            </div>

            <div class="col-md-2 text-right">

                <strong>30 Oct</strong><br>
                <small>2016</small>

            </div>

            <div style="margin: 100px 0"></div>

        </div>

    </div><!--- Container end -->
</section>

@endsection
