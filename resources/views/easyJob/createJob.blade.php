@extends('layouts.master')
@section('content')

    <!--- Banner image --->

    {{--<section id="find-job-banner">--}}


    {{--<div class="find-job-note">--}}

    {{--<h2 style="font-weight: 100;">Find Your <strong>Dream Job</strong></h2>--}}

    {{--<h2 style="letter-spacing: 65px;">ONLINE</h2>--}}

    {{--</div>--}}

    {{--</section>--}}


    <div class="jumbotron" style="margin: 0;">

        <div class="container">
            <div class="row">
                @if(Auth::check())


                    @if(count($companies) == 0)
                        <div class="alert alert-danger">
                            <strong>Register your company to post a job</strong>
                        </div>
                    @endif

                        <div class="col-md-12 ">

                            <form action="{{action('easyJobController@postCreateJob')}}" method="post">
                                {{csrf_field()}}
                                <div class="col-md-6 ">

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label">Job Title: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="text" name="title" placeholder="plumber" id="">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label">Skill: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="text" name="skill" placeholder="Plumber / Electrician" id="">
                                            @if ($errors->has('skill'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('skill') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    {{--<div class="form-group row">--}}
                                    {{--<label for="" class="col-xs-12 col-form-label"> Total Positions: </label>--}}
                                    {{--<div class="col-xs-10">--}}
                                    {{--<input class="form-control" type="text" name="position" placeholder="1 Post" id="">--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> City: </label>
                                        <div class="col-xs-10">

                                            <select class="form-control" name="city">
                                                <option>Lahore</option>
                                                <option>Karachi</option>
                                                <option>Islamabad</option>
                                                <option>Faislabad</option>
                                                <option>Multan</option>
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> Job Type: </label>
                                        <div class="col-xs-10">
                                            <select class="form-control" name="jobType">
                                                <option vlaue ="weekly">Weekly</option>
                                                <option vlaue ="hourly">Hourly</option>
                                                <option vlaue ="daily">Daily</option>
                                                <option vlaue ="monthly">Monthly</option>
                                                @if ($errors->has('jobType'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('jobType') }}</strong>
                                            </span>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label">Address: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="text" name="address"
                                                   placeholder="AlNafay Tech, Main Market, Lahore Pakistan" id="">
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label">Company Name: </label>
                                        <div class="col-xs-10">

                                            <select class="form-control" name="companyId">
                                                @foreach($companies as $company)
                                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('companyId'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('companyId') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>


                                <div class="col-md-6 ">

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> Experience: </label>
                                        <div class="col-xs-10">
                                            <select class="form-control" name="experience" id="">
                                                <option value="fresher">Fresher</option>
                                                <option value="one year"> 1 year</option>
                                                <option value="two year">2 years</option>
                                                <option value="more">More than 2</option>
                                                </select>
                                            @if ($errors->has('experience'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('experience') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> Age: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="text" name="age"
                                                   placeholder="Minimum 18 Years" id="">
                                            @if ($errors->has('age'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('age') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label">Gender: </label>
                                        <div class="col-xs-10">
                                            <select class="form-control" type="text" name="gender" id="">
                                                <option value="Male">Male</option>
                                                <option value="female">Female</option>
                                                @if ($errors->has('gender'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                            </span>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> Job Rate: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="text" name="jobRate"
                                                   placeholder="1000 / 1200" id="">
                                            @if ($errors->has('jobRate'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('jobRate') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="" class="col-xs-12 col-form-label"> Apply Before: </label>
                                        <div class="col-xs-10">
                                            <input class="form-control" type="date" name="applyBefore" id="">
                                            @if ($errors->has('apply-before'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('apply-before') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-xs-12 col-form-label"> Job Description: </label>
                                    <textarea class="form-group ajax-content" style="width: 1015px;" rows="3" id=""
                                              name="jobDescription" placeholder="Job Description">
                            </textarea>

                                    <p>By clicking Next you accept our <a href="#">Conditions of Use</a></p>
                                    <a href="{{ url('job-preview') }}">

                                        @if(count($companies) > 0)
                                            <button class="btn btn-info btn-md" style="width: 250px;">Create Job</button>

                                            @else
                                            <button class="btn btn-info btn-md" style="width: 250px;" disabled>Create Job</button>
                                            @endif
                                    </a>
                                </div>
                            </form>
                        </div>

                @else

                    <div class="jumbotron" style="margin: 0;">

                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-8">
                                        <p>If You do not have an account. First Register yourself<a
                                                    href="{{ route('sign-up') }}"><strong>  Sign Up</strong></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <!--- Message Section --->
                        <section id="job-message">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-center">Live Stats: 2982 Jobs are currently OPEN</p>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--- Job Desc --->
                        <hr>
                        <div class="container">

                            <div class="row">

                                <div class="col-md-4">

                                    <img src="{{asset('images/workers.jpg')}}">

                                </div>

                                <div class="col-md-8">

                                    <h4>It is a long established fact that a reader will be distracted by the readable content of a page
                                        when looking at its layout.</h4>

                                    <h5>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as
                                        opposed to using 'Content here, content here', making it look like readable English. Many
                                        desktop publishing packages and web page editors now use Lorem Ipsum as their default model
                                        text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
                                        versions have evolved over the years, sometimes by accident, sometimes on purpose (injected
                                        humour and the like).</h5>

                                    <p class="text-right">Steve TV<br> Plumbing Pty Ltd</p>

                                </div>

                            </div>

                        </div>

                        <hr>

                        {{--<span style="display: block;  position: relative; bottom: 60px; font-size: 55px;" class="text-center"><a href="#"><i class="fa fa-arrow-circle-down"></i></a></span>--}}

                        <div class="container">

                            <div class="row">

                                <div class="col-md-8">

                                    <br><br>

                                    <h4>It is a long established fact that a reader will be distracted by the readable content of a page
                                        when looking at its layout.</h4>

                                    <h5>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as
                                        opposed to using 'Content here, content here', making it look like readable English. Many
                                        desktop publishing packages and web page editors now use Lorem Ipsum as their default model
                                        text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
                                        versions have evolved over the years, sometimes by accident, sometimes on purpose (injected
                                        humour and the like).</h5>

                                </div>

                                <div class="col-md-4">

                                    <div class="thumbnail">

                                        <img src="{{asset('images/movt-mes.jpg')}}" class="img img-responsive">

                                    </div>

                                </div>

                            </div>

                        </div>
                        @endif

                    </div>
            </div>
        </div>
    </div>
@endsection
