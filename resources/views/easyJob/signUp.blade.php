@extends('layouts.master')
@section('content')

<!--- Login Section Start --->

<section id="login-section">

    <div class="container">



        <div class="row">

            <div class="col-md-12">

                <div class="form-box">
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <h4>Remote Worker Registeration</h4>
                        <div class="row">

                            <div class="col-md-12 col-sm-12">
                                <a href="{{route('login')}}"><input type="submit" value="Login" class="form-control button"></a>
                            </div>

                        </div>

                    <p><i class="fa fa-user" aria-hidden="true"></i></p>


                    <form method="post" action="{{ action('userController@postSignUp') }}">
                    {{ csrf_field() }}

                        <div class="inner-addon left-addon {{ $errors->has('email') ? ' has-error' : '' }}">
                            <i class="glyphicon glyphicon-envelope"></i>
                            <input type="email" name="email" class="form-control" placeholder="Email:" requiredF>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                            @endif
                        </div>

                        <br>

                        <div class="inner-addon left-addon{{ $errors->has('password') ? ' has-error' : '' }}">
                            <i class="glyphicon glyphicon glyphicon-lock"></i>
                            <input type="password" name="password" class="form-control" placeholder="Password: " required />
                            @if ($errors->has('password'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                            @endif
                        </div>
                        <br>
                        <div class="inner-addon left-addon{{ $errors->has('confirmPassword') ? ' has-error' : '' }}">
                            <i class="glyphicon glyphicon-lock"></i>
                            <input type="password" name="confirmPassword" class="form-control" placeholder="Confirm Password: " required />
                            @if ($errors->has('confirmPassword'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('confirmPassword') }}</strong>
                                            </span>
                            @endif
                        </div>
        <br>
                        <div class="inner-addon left-addon{{ $errors->has('role') ? ' has-error' : '' }}">
                            <select name="role" class="form-control ">
                                <option value="employer">Employeer</option>
                                <option value="job seeker">Job Seeker</option>
                            </select>
                            {{--selectpicker--}}
                        @if ($errors->has('role'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="row">

                            <div class="col-md-12 col-sm-12">

                                <input type="submit" value="Sign Up" class="form-control button">

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection

