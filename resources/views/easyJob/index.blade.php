@extends('layouts.master')
@section('content')
<!--- Main Banner Image Start --->

<section id="main-image">

    <div class="jumbotron" id="main-banner">

        <div class="container">

            <div class="row text-center">

                <div class="col-md-12">

                    <h1 style="color: #FFF;">WE GET JOBS DONE</h1>

                    <form class="">

                        <div class="row">

                            <div class="col-md-5 col-sm-12">

                                <div class="form-group">

                                    <input type="text" placeholder="What do you need? (e.g. designer, mechanic)" class="form-control">

                                </div>

                            </div>

                            <div class="col-md-5 col-sm-12">

                                <div class="form-group">

                                    <input type="text" placeholder="Where are you located? (Postcode or suburb)" class="form-control">

                                </div>



                            </div>

                            <div class="col-md-2">

                                <div class="form-group">

                                    <button class="btn btn-info form-control" id="search-btn">Search</button>

                                </div>

                            </div>

                        </div>

                        <p>Feel Free To Contact Us: +92303-1112223</p>

                    </form>

                </div>

            </div>

        </div>

    </div>

</section>

<!--- Image Banner End --->

<!--- Services Start --->

<section id="services-section">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h2>MOST REQUESTED SERVICES</h2>

            </div>

        </div>

        <div class="row text-center">

            <div class="col-md-4">

                <div class="service-box">

                    <p><i class="fa fa-pencil"></i></p>

                </div>

                <p><strong>Architect</strong></p>

            </div>

            <div class="col-md-4">

                <div class="service-box">

                    <p><i class="fa fa-wrench"></i></p>

                </div>

                <p><strong>Plumber</strong></p>

            </div>

            <div class="col-md-4">

                <div class="service-box">

                    <p><i class="fa fa-plug"></i></p>

                </div>

                <p><strong>Electrician</strong></p>

            </div>

        </div>

    </div>

</section>

<!--- Services End --->

<!--- Describe Services Section Start --->

<section id="desc-services">

    <div class="container-fluid">

        <div class="row text-center">

            <div class="col-md-12">

                <hr id="line">

                <span class="line-heading">Architect</span>

            </div>

        </div>


        <div class="row text-center">

            <div class="col-md-6">

                <img src="{{asset('images/test-1.jpg')}}" class="img img-responsive" id="main-imgs">

            </div>

            <div class="col-md-6 text-left">

                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

            </div>

        </div>

    </div>

    <div class="container-fluid">

        <div class="row text-center">

            <div class="col-md-12">

                <hr id="line">

                <span class="line-heading">Plumber</span>

            </div>

        </div>


        <div class="row text-center">

            <div class="col-md-6">

                <img src="{{asset('images/test-2.jpg')}}" class="img img-responsive" id="main-imgs">

            </div>

            <div class="col-md-6 text-left">

                <p>Look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

            </div>

        </div>

    </div>

    <div class="container-fluid">

        <div class="row text-center">

            <div class="col-md-12">

                <hr id="line">

                <span class="line-heading">Electronic</span>

            </div>

        </div>


        <div class="row text-center">

            <div class="col-md-6">

                <img src="{{asset('images/test-1.jpg')}}" class="img img-responsive" id="main-imgs">

            </div>

            <div class="col-md-6 text-left">

                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

            </div>

        </div>

    </div>

</section>

<hr style="margin: 50px 0;">

<!--- Describe Services Section End --->

<!--- Contact Area Start --->

<section id="contact-section">

    <div class="container">

        <div class="row text-center">

            <div class="col-md-12">

                <h2>Contact Our Customers Team Support</h2>

                <span style="background-color: #28abe3; width: 230px; display:inline-block; padding: 3px;"> </span>
                <span style="background-color: #777; width: 100px; display: inline-block; padding: 3px;"> </span>
                <span style="background-color: #AAA; width: 133px; display: inline-block; padding: 3px;"> </span>

            </div>

        </div>

        <div class="row text-center" id="contact-icon-section">

            <div class="col-md-4">

                <p><i class="fa fa-phone icon-style"></i></p>

                <p><strong>Phone</strong><br> +123456</p>

            </div>

            <div class="col-md-4">

                <p><i class="fa fa-clock-o icon-style"></i></p>

                <p><strong>Opening Hours</strong><br>Mon - Fri <small>8:00am to 6:00pm</small></p>

            </div>

            <div class="col-md-4">

                <p><i class="fa fa-envelope icon-style"></i></p>

                <p><strong>Email us</strong><br>info@easyjobs.com</p>

            </div>

        </div>

    </div>

</section>

<!--- Contact Area End --->

@endsection