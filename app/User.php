<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public function companies(){

        return $this->hasMany('App\Company');
    }

    public function resume(){
        return $this->hasMany('App\Resume');
    }

    public function experience(){
        return $this->hasMany('App\Experience');
    }

    public function education(){
        return $this->hasMany('App\Education');
    }

    public function job(){
        return $this->belongsToMany('App\Job', 'job_user_quote');
    }

    public function roles(){
        return $this->belongsToMany('App\Role', 'role_user_quote');
    }
}
