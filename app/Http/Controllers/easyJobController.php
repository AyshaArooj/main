<?php

namespace App\Http\Controllers;


use App\Company;
use App\User;
//use App\Http\Requests\Request;
use App\Job;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class easyJobController extends Controller
{
    public function index()
    {
        return view('easyJob.index');
    }

    public function getJobList()
    {
        return view('easyJob.jobList');
    }


    public function findJob()
    {
        return view('easyJob.findJob');
    }


    public function getcreateJob()
    {
        $companies = DB::table('companies')->where('user_id', Auth::user()->id)->get();
        return view('easyJob.createJob', ['companies'=> $companies]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreateJob(Request $request){

        $this->validate($request, [
            'title' => 'bail|required:min:3',  ////// job_title //////
            'skill' => 'bail|required',    ////// skill //////
//            'position' => 'required',
            'city' => 'required',       ///// city /////
            'address' => 'required',    ////// address ////
            'jobType' => 'required',
            'jobRate' => 'required',         //////job Rate /////
//            'career-level' => 'required',
            'gender' => 'required',
            'experience' => 'required',      //// experience /////
//            'mobile' => 'required',
//            'age' => 'required',
            'applyBefore' => 'required',
//            'posting-date' => 'required',
            'jobDescription' => 'required',
            'companyId' => 'required'
            ]);

        $createJob = new Job();
        $createJob->user_id = Auth::user()->id;
        $createJob->job_title = $request->title ;
        $createJob->skill = $request->skill ;
        $createJob->city = $request->city;
        $createJob->address = $request->address ;
        $createJob->job_type = $request->jobType ;
        $createJob->job_rate = $request->jobRate;
        $createJob->gender = $request->gender;
        $createJob->experience = $request->experience;
        $createJob->apply_before= $request->applyBefore;
        $createJob->description = $request->jobDescription;
        $createJob->company_id= $request->companyId;
        $createJob->save();

        return redirect()->route('job-preview', $createJob->id);
    }

    public function jobPreview ($id)
    {
        $job = Job::findOrFail($id);
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        return view('easyJob.jobPreview', compact('job', 'user'));
    }

  public function dashboard()
  {
      $user = DB::table('users')->where('email',Auth::user()->email)->first();

      return view('easyJob.userDashboard', ['user' => $user]);
  }

    public function profile()
    {
        return view('easyJob.userProfile');
    }

    public function jobDetail(){

        $jobDetail = DB::table('jobs')->where('user_id', Auth::user()->id)->first();
//dd($jobDetail);
        return view('easyJob.jobDetail', ['jobDetail' => $jobDetail]);
    }

    public function applyJob(){
        echo ('Success! Your application is successfuly submited against this job');
        return true;

//        if ($id = Auth::user()->id){
//            return redirect()->to('easyJob.jobDetail')->with('Success', 'Your application is successfuly submited against this job');
//        }
//        else{
//            return redirect()->to('easyJob.jobDetail')->with('Fail', 'Your application is not submited against this job');
//        }
    }

    public function jobList(){

        $joblists = DB::table('jobs')->where('user_id',Auth::user()->id)->get();
        return view('easyJob.jobList', ['jobLists' => $joblists]);

    }

    public function getAddCompany(){

        return view('easyJob.registerCompany');
    }

    public function postAddCompany(Request $request){

        $this->validate($request, [
            'c-name' => 'bail|required',
            'industry' => 'bail|required',
            'ceo' => 'bail|required',
            'head-hrd' => 'bail|required',
            'designation' => 'bail|required',
            'contact-email' => 'bail|required',
            'contact-person' => 'bail|required',
            'company-url' => 'bail|required',
            'c-product' => 'bail|required',
            'ownership-type' => 'bail|required',
            'c-address' => 'bail|required',
            'c-location' => 'bail|required',
            'c-origin' => 'bail|required',
            'office-count' => 'bail|required',
            'c-phone' => 'bail|required',
            'c-fax' => 'bail|required',
            'workers-count' => 'bail|required',
            'operating-years' => 'bail|required',
            'logo' => 'bail|required',
            'secp-id' => 'bail|required',
            'c-description' => 'bail|required',
            ]);

        $createCompany = new Company();
        $createCompany->user_id = Auth::user()->id;
        $createCompany->name = $request ['c-name'];
        $createCompany->industry = $request->industry;
        $createCompany->ceo_name = $request->ceo;
        $createCompany->hr_name = $request ['head-hrd'];
        $createCompany->contact_person = $request ['contact-person'];
        $createCompany->job_designation = $request->designation;
        $createCompany->ownership = $request ['ownership-type'];
        $createCompany->product = $request ['c-product'];
        $createCompany->address = $request ['c-address'];
        $createCompany->location = $request ['c-location'];
        $createCompany->origin_country = $request ['c-origin'];
        $createCompany->email =$request ['contact-email'];
        $createCompany->phone_no = $request ['c-phone'];
        $createCompany->company_url = $request ['company-url'];
        $createCompany->no_of_offices = $request ['office-count'];
        $createCompany->no_of_employees = $request ['workers-count'];
        $createCompany->year_of_operating = $request ['operating-years'];
        $createCompany->fax = $request ['c-fax'];
        $createCompany->logo = $request->logo;
        $createCompany->description = $request ['c-description'];
        $createCompany->save();

        return redirect()->to('create-job');
    }
//
}
