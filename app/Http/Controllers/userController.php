<?php
/**
 * Created by PhpStorm.
 * User: AlNafay Tech
 * Date: 11/16/2016
 * Time: 12:45 PM
 */

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\User;
//use Validator;
use Auth;

class userController extends Controller
{
    public function getlogin()
    {
        return view('easyJob.login');
    }
    public function postLogin(Request $request)
    {
        $this->validate($request,[
           'email' =>  'required|email',
            'password' => 'required|string'
        ]);
        $data=[
            'email' => $request->email,
            'password' => $request->password
        ];
        if(Auth::attempt($data)){
            return redirect()->to('dashboard');
        }
        else{
            return redirect()->back()->with('fail','Invalid login credentials');
        }
///////it will take data from the user through request if it is valid then it take(get) that data into an array, through attempt
//////////method check the array and will redirect to index page else show fail message////////////////
    }

    public function getSignUp()
    {
        return view('easyJob.signUp');
    }

    public function postSignUp(Request $request)
    {
        $this->validate($request,[
            'email'=> 'required|email|unique:users,email|max:250',
            'password'=> 'required',
            'confirmPassword' => 'required|same:password',
            'role' => 'required'
        ]);

        $role = Role::where('name', $request['role'])->first();

        $user = new User();
        $user->email= $request->email;
        $user->password= bcrypt($request->password);
        $user->save();

        $user->roles()->save($role, ['status' => 1]);
         return redirect()->back()->with('success', 'Your account has been created Successfully !');

    }

    public function logout(){
        Auth::logout();
        return redirect()->to('/');
    }
}

