<?php


Route::get('/', 'easyJobController@index');


Route::get('/login', [
    'uses' => 'userController@getLogin',
    'middleware' => 'authed',
    'as' => 'login'
]);

Route::post('/login', 'userController@postLogin');

Route::get('/logout', [
    'uses' => 'userController@logout',
    'middleware' => 'auth'
]);

Route::get('/sign-up', [
    'uses' => 'userController@getSignUp',
    'middleware' => 'authed'
]);
Route::post('/sign-up', 'userController@postSignUp');

Route::get('/add-company', 'easyJobController@getAddCompany');
Route::post('/add-company', 'easyJobController@postAddCompany');

Route::get('/job-list', 'easyJobController@getJobList');
Route::get('/job-list', 'easyJobController@jobList');

Route::get('/find-job', 'easyJobController@findJob');

Route::get('/create-job', 'easyJobController@getcreateJob');
Route::post('/create-job', 'easyJobController@postCreateJob');

Route::get('/job-preview/{id}', [
    'uses' => 'easyJobController@jobPreview',
    'as' => 'job-preview'
]);

Route::get('/dashboard', [
    'uses' => 'easyJobController@dashboard',
    'as' => 'dashboard']);

Route::get('/profile', 'easyJobController@profile');

Route::get('/job-detail','easyJobController@jobDetail');






//Route::get('login',['as'=>'login', 'uses'=>'easyJobController@login']);
