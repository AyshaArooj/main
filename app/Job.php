<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{

//    use SoftDeletes;

    protected $fillable =
        ['created_at', 'updated_at', 'apply_before'];

    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function user(){
        return $this->belongsToMany('App\User', 'job_user_quote');
    }
}
